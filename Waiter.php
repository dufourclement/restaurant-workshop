<?php
function printMenu($client){
    if( $client["staterIds"]->length === 0) {
        $starterDisplayedText = 'Rien';
    } else {
        $starters = [];
        foreach($client["staterIds"] as $starterId){
            $starters[]= getDish($starterId)["name"];
        }
        $starterDisplayedText = implode(", ", $starters);
    }
    if( $client["mainIds"]->length === 0) {
        $mainDisplayedText = 'Rien';
    } else {
        $mains = [];
        foreach($client["mainIds"] as $mainId){
            $mains[]= getDish($mainId)["name"];
        }
        $mainDisplayedText = implode(", ", $mains);
    }
    if( $client["dessertIds"]->length === 0) {
        $dessertDisplayedText = 'Rien';
    } else {
        $desserts = [];
        foreach($client["dessertIds"] as $dessertId){
            $desserts[]= getDish($dessertId)["name"];
        }
        $dessertDisplayedText = implode(", ", $desserts);
    }
    print("Entrée(s) choisie(s):$starterDisplayedText \n");
    print("Plat(s) choisi(s): $mainDisplayedText \n");
    print("Dessert(s) choisi(s): $dessertDisplayedText \n");
}
function takeOrder($client) {
    sleep(0.001);
    print("Nom du Client: " . $client["name"] . "\n");
    printMenu($client);
    return ["staterIds" => $client["staterIds"], "mainIds" => $client["mainIds"], "dessertIds" => $client["dessertIds"] ];
}
function serveFood(){
    sleep(0.001);
}
function clearTable(){
    sleep(0.001);
}
?>