<?php
    function getElementById($arr, $id) {
        foreach($arr as $element) {
            if($element['id'] == $id) {
                return $element;
            }
        }
    }
    function getDishes() {
        $data = json_decode(file_get_contents('data.json'), true);
        return $data["dishes"];
    }
    function getDish($dishId) {
        return getElementById(getDishes(), $dishId);
    }
?>