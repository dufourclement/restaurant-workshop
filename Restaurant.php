<!-- 
    Cas 1: Préparer un repas, servir, laisser le temps de manger, débarasser la table et faire la vaisselle.
    Cas 2: Récupérer la commande d'un client avec Plusieurs plats
    Cas 3: Type de plats et ordre
    Cas 4: Plusieurs clients
    Cas 5: Demander les desserts a la fin du repas principal
    Cas 6: Ajouter un temps pour laisser le client réfléchir
    Vitesses différentes en fonction des clients ou de l'expérience des cuisiner, serveur, plongeur? (peu pertinent?)

    Long flow -> Dresser la table?, donner menus?, temps de reflexion (client)?, prendre commande, cuisiner, servir, manger, débarrasser la table, faire la vaisselle.
 -->

<?php

include "Waiter.php";
include "Cook.php";
include "Dishwasher.php";
include "Client.php";
include "Data.php";

$start_time = microtime(true);
//Cas 1
// $dishesId = 1;
// cook($dishId);
// serveFood();
// eat($dishId);
// clearTable();
// washDishes();

//Cas 2
// $client = ['id' => 10 , "name" => 'Didier', "dishesIds" => [1,2,3,4]];
// $dishesIds = takeOrder($client);
// foreach ($dishesIds as $dishId) {
//     cook($dishId);
//     serveFood();
//     eat($dishId);
//     clearTable();
//     washDishes();
// }

//Cas 3
// $client = ['id' => 10 , "name" => 'Didier', "staterIds" => [1,2], "mainIds" => [4, 6], "dessertIds" => [9]];
// $dishesIdsByType = takeOrder($client);
// foreach ($dishesIdsByType['staterIds'] as $dishId) {
//     cook($dishId);
// }
// serveFood();
// eat($dishId);
// clearTable();
// washDishes();

// foreach ($dishesIdsByType['mainIds'] as $dishId) {
//     cook($dishId);
// }
// serveFood();
// eat($dishId);
// clearTable();
// washDishes();

// foreach ($dishesIdsByType['dessertIds'] as $dishId) {
//     cook($dishId);
// }
// serveFood();
// eat($dishId);
// clearTable();
// washDishes();

// Cas 4:
$clients = [['id' => 9 ,"staterIds" => [3], "mainIds" => [5], "dessertIds" => [10]] ,['id' => 10 ,"staterIds" => [1,2], "mainIds" => [4, 6], "dessertIds" => [9]]];
foreach ($clients as $client) {
    $dishesIdsByType = takeOrder($client);
    foreach ($dishesIdsByType['staterIds'] as $dishId) {
        cook($dishId);
    }
    serveFood();
    eat($dishId);
    clearTable();
    washDishes();

    foreach ($dishesIdsByType['mainIds'] as $dishId) {
        cook($dishId);
    }
    serveFood();
    eat($dishId);
    clearTable();
    washDishes();

    foreach ($dishesIdsByType['dessertIds'] as $dishId) {
        cook($dishId);
    }
    serveFood();
    eat($dishId);
    clearTable();
    washDishes();
}
$end_time = microtime(true);
$execution_time = ($end_time - $start_time);
print("\n\nTEMPS D'EXECUTION: Vous avez mis $execution_time secondes à servir vos clients.\n");
?>


